const tagParagraph = document.querySelectorAll("p");
tagParagraph.forEach((element) => {
  element.style.backgroundColor = "red";
  console.log(element);
});
console.log(tagParagraph);
const elemById = document.getElementById("optionsList");
console.log(elemById);
const elemByIdParrent = document.getElementById("optionsList").parentNode;
console.log(elemByIdParrent);
const elemByIdChild = document.getElementById("optionsList").childNodes;
console.log(elemByIdChild);
let p = document.createElement("p");
p.className = "testParagraph";
p.innerHTML = "<p>This is a paragraph</p>";
document.body.append(p);
const elemLi = document.querySelectorAll(".main-header li");
elemLi.forEach((element) => {
  element.classList.add("nav-item");
});

console.log(elemLi);

const searchElemByClass = document.querySelectorAll(".section-title");
searchElemByClass.forEach((element) => {
  element.classList.remove("section-title");
});
console.log(searchElemByClass);