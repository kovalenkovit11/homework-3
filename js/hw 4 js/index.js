// ## Теоретический вопрос

// 1. Описать своими словами для чего вообще нужны функции в программировании.
// Для того что бы не повторялся одинаковый код.
// 2. Описать своими словами, зачем в функцию передавать аргумент.
// Для того что бы потом эти аргумент можно было проще конвертировать в массив.
// 3. Что такое оператор return и как он работает внутри функции?
// Завершает выполнения функции и возвращает результат.

// ## Задание

// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Считать с помощью модального окна браузера два числа.
// - Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено `+`, `-`, `*`, `/`.
// - Создать функцию, в которую передать два значения и операцию.
// - Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:

// - После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

// #### Литература:

// - [Функции - основы](https://learn.javascript.ru/function-basics)

function readNumber() {
  let num = +prompt('Please, enter a number');
  while (Number.isNaN(num)) {
    num = +prompt('Not a number, please, retry');
  }

  return num;
}

const number1 = readNumber();
const number2 = readNumber();
const userOperation = prompt(
  "Please youre mathematical operation you can write  `+`, `-`, `*`, `/` "
);
while (
  userOperation !== "+" &&
  userOperation !== "-" &&
  userOperation !== "*" &&
  userOperation !== "/" 

) {
  userOperation = prompt("Please Mathematical Operation `+`, `-`, `*`, `/`");
}
function calc(number1, number2, userOperation){
  if(userOperation === "+"){
    return number1 + number2
  }
  if(userOperation === "-"){
    return number1 - number2
  }
  if(userOperation === "*"){
    return number1 * number2
  }if(userOperation === "/"){
    return number1 / number2
  }
}
console.log(calc(number1, number2, userOperation));


// function calkMathematicalOperationPlus(number1, number2){
//   let sum = (number1 + number2 )
//   console.log(sum)
// }

// if(userOperation === "+"){
//   calkMathematicalOperationPlus(number1, number2)
// }

// function calkMathematicalOperationMinus(number1, number2){
//   let sum = (number1 - number2 )
//   console.log(sum)
  
// }

// if(userOperation === "-"){
//   calkMathematicalOperationMinus(number1, number2)
// }
// function calkMathematicalOperationMultiplication(number1, number2){
//   let sum = (number1 * number2 )
//   console.log(sum)
  
// }
// if(userOperation === "*"){
//   calkMathematicalOperationMultiplication(number1, number2)
// }
// function calkMathematicalOperationDivision(number1, number2){
//   let sum = (number1 / number2 )
//   console.log(sum)
  
// }if(userOperation === "/"){
//   calkMathematicalOperationDivision(number1, number2)
// }
