(function ($) {
  $(function () {
    $("ul.tabs__caption").on("click", "li:not(.active)", function () {
      $(this)
        .addClass("active")
        .siblings()
        .removeClass("active")
        .closest("div.tabs")
        .find("div.tabs__content")
        .removeClass("active")
        .eq($(this).index())
        .addClass("active");
    });
  });
})($);

function createNewImg() {
  $('#loadMoreButton').remove();
  $('.preloader').css('display','block');
  setTimeout(getMoreImgs, 2000);
  function getMoreImgs(){
      $('.displayNone').removeClass('displayNone');
      $('.categoryItemsSecond').attr('class','categoryItems');
      $('.categoryItems').css('display','block');
      $('.categoryItemsSecond').removeClass('.categoryItemsSecond');

      $('.contentAmazing').css('min-height','850px');
      $('.contentAmazing').css('height','auto');
      $('.preloader').css('display','none');
  }

}
$('#loadMoreButton').on('click',createNewImg);

