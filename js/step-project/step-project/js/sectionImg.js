$(document).ready(function() {
$('.main_tabs__caption li').click(function(event){
  event.preventDefault();
  $('.main_tabs__caption li').removeClass('active');
  $(this).addClass('active');
  let imgWidth = '285px';
  let imgHeight = '210px';
  let thisItem = $(this).attr('rel');
  if(thisItem !== 'all'){
$('.item li[rel='+thisItem+']').stop()
.animate({
  'width' : imgWidth,
  'height' : imgHeight,
  'display' : 'flex',
 'marginRight' : 0,
  'marginLeft' : 0
});
          $('.item li[rel!='+thisItem+']').stop()
              .animate({'width' : 0,
                  'height' : 0,
                  'opacity' : 1,
                  'marginRight' : 0,
                  'marginLeft' : 0,
                  'display' : 'none'

              });

  } else{
    $('.item li').stop()
    .animate({'display' : 'block',
    'opasity' : 1,
        'height' : imgHeight,
        'marginRight' : 0,
        'marginLeft' : 0,
        'width' : imgWidth,
    });
  }

})
$('.img_hover_items img').animate({'opacity' : 1}).hover(function() {
  $(this).animate({'opacity' : 1});
}, function() {
  $(this).animate({'opacity' : 1});
});



// $('.slider').slick({
//   arrows: false,
//   autoplay: false,
//   asNavFor: '.slider_second',
//   infinite:true,
//   focusOnSelect:true,
//   // centerMode: true;
// });
// $('.slider_second').slick({
//   arrows: true,
//   autoplay: false,
//   slidesToShow: 4,
//   asNavFor: '.slider',
//   infinite:true,
//   focusOnSelect: true,
//   centerMode:true,
//   // draggable:false,
//   variableWidth:true,
// })


// $('.slider-for').slick({
//   infinite: true,
//   slidesToShow: 2,
//   slidesToScroll: 1
// });
 $('.slider-for').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: false,
  arrows: false,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
    prevArrow: '.myArrowLeft',
  nextArrow: '.myArrowRight',
  centerMode: true,
  focusOnSelect: true,
// variableWidth: true,
// variableHeight: true,
  

 });
// $('.slider-nav').slick({
//   dots: false,
//   centerMode: true,
//   arrows:true,
//   prevArrow: '.myNawLeft',
//   nextArrow: '.myNawRight',
//   infinite: true,
//   speed: 300,
//   slidesToScroll: 1,
//   // slidesToShow: 4,
//   adaptiveHeight: true,
//   asNavFor:'.slider-for',
//   focusOnSelect: true,
//   variableHeight: true,
//   variableWidth: true
// });
})
