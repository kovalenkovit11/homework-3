// ## Задание

// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
// - Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
// - Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`. Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:

// - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.

// function createNewUser() {
//   this.userName = prompt('Enter you name: ','');
//   while (this.userName === ''){
//       this.userName = prompt('Enter you name AGAIN: ','');
//   }

//   this.userSecondName = prompt('Enter you second name','');
//   while (this.userSecondName === ''){
//       this.userSecondName = prompt('Enter you second name AGAIN: ','');
//   }

//   this.getLogin = function(){
//       let newLogin = this.userName.charAt(0).toLowerCase() + this.userSecondName.toLowerCase();
//       return newLogin;
//   }
// }

// let newUserObj = new createNewUser();
// console.log(`Your login is: ${newUserObj.getLogin()}`);

//2 Variant
let userName = prompt("Enter you name: ", "");
while (userName === "") {
  userName = prompt("Enter you name AGAIN: ", "");
}
let userSecondName = prompt("Enter you last name: ", "");
while (userSecondName === "") {
  userSecondName = prompt("Enter you second name AGAIN: ", "");
}
function createNewUser() {
  const newUser = {
    userName,
    userSecondName,
    getLogin: function () {
      let newLogin =
        this.userName.charAt(0).toLowerCase() +
        this.userSecondName.toLowerCase();
      return newLogin;
    },
  };
  return newUser;
}
let newUserObj = createNewUser();
console.log(`Your login is: ${newUserObj.getLogin()}`);
