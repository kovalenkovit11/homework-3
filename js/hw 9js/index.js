// ## Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// - кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:

// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можна взяти будь-який інший масив.

// #### Необов'язкове завдання підвищеної складності:

// 1. Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список.
//    Приклад такого масиву:

//    ```javascript
//    ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//    ```

//    > Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

// 2. Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const newArr = `<ul>${arr.map((elem) => `<li>${elem}</li>`)}</ul>`;
let splitAndJoin = newArr.split(",").join("");
const div = document.createElement("div");
div.innerHTML = splitAndJoin;
document.body.append(div);

let timer = 3;
function timeSec() {
  
  if (timer < 1) {
    document.body.innerHTML = '';
  }else{
    document.getElementById('timer').innerHTML = timer;
    setTimeout(timeSec, 1000)
    timer--;
  }
}
timeSec()
