const tabsName = document.querySelector(".tabs")



const tabsTitleList = document.querySelector('.tabs');

tabsTitleList.addEventListener('click', (event) => {
    if (event.target.classList.contains('tabs-title')) {
        const attr = event.target.dataset.tab;
        document.querySelector('.tabs-title.active').classList.remove('active');
        event.target.classList.add('active');
        document.querySelector('.tab-content.active').classList.remove('active');
        document.querySelector(`[data-content=${attr}-content]`).classList.add('active');
    }
});


