// ## Задание

// Дополнить функцию `createNewUser()` методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее следующим функционалом:
//   1.  При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и сохранить ее в поле `birthday`.
//   2.  Создать метод `getAge()` который будет возвращать сколько пользователю лет.
//   3.  Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
// - Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()` созданного объекта.

let userName = prompt("Enter you name: ", "");
while (userName === "") {
  userName = prompt("Enter you name AGAIN: ", "");
}
let userSecondName = prompt("Enter you last name: ", "");
while (userSecondName === "") {
  userSecondName = prompt("Enter you second name AGAIN: ", "");
}
function createNewUser() {
  const newUser = {
    userName,
    userSecondName,
    getLogin: function () {
      let newLogin =
        this.userName.charAt(0).toLowerCase() +
        this.userSecondName.toLowerCase();
      return newLogin;
    },
  };
  return newUser;
}
let newUserObj = createNewUser();
// console.log(`Your login is: ${newUserObj.getLogin()}`);

newUserObj.userBirthday = prompt("how old are you", `dd.mm.yyyy`);
// console.log(newUserObj)

newUserObj.getAge = function () {
  let userAge = 2022 - newUserObj.userBirthday.split(`.`)[2];
  return userAge;
};
// console.log(newUserObj.getAge())
newUserObj.getPassword = function () {
  let newPassword =
    newUserObj.userName.charAt(0).toUpperCase() +
    newUserObj.userSecondName.toLowerCase() +
    newUserObj.userBirthday.split(`.`)[2];
  return newPassword;
};
console.log(newUserObj);
console.log(newUserObj.getAge());
console.log(`Your password is: ${newUserObj.getPassword()}`);
console.log(`Your login is: ${newUserObj.getLogin()}`);
